/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.oxprogram2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author a
 */
public class OxProgram2Test {

    public OxProgram2Test() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckVerticleOCol1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'O', '-', '-'}, 
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;

        assertEquals(true, OXProgram2.checkVertical(table, currentPlayer, col));
    }
    
    
    @Test
    public void testCheckVerticleOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;

        assertEquals(true, OXProgram2.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticleOCol3NoWin() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', '-', 'O'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;

        assertEquals(false, OXProgram2.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckHorizontalXRow1Win() {
        char table[][] = {{'X', 'X', 'X'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;

        assertEquals(true, OXProgram2.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalXRow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', '-', '-'}, 
                                 {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;

        assertEquals(true, OXProgram2.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalXRow2NoWin() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'X', 'X', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;

        assertEquals(false, OXProgram2.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckX1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';

        assertEquals(true, OXProgram2.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2NoWin() {
        char table[][] = {{'-', '-', 'X'}, 
                                 {'-', '-', '-'}, 
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';

        assertEquals(false, OXProgram2.checkX2(table, currentPlayer));
    }
    
    @Test
    public void testCheckDraw() {
        char table[][] = {{'O', 'X', 'X'}, 
                                 {'X', 'O', 'X'}, 
                                 {'X', 'X', 'O'}};
        
        int count = 8;
        
        assertEquals(true, OXProgram2.checkDraw(count));
    }
    
}
