/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.oxprogram2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author a
 */
public class TDDTest {
    
    public TDDTest() {
    }
    
     @Test
     public void testAdd_1_2is3() {
         assertEquals(3, Example.add(1, 2));
     
     }
     
     @Test
     public void testAdd_15_20is35() {
         assertEquals(35, Example.add(15, 20));
     
     }
     
     @Test
     public void testChop_p1P_p_p2P_is_draw(){
         assertEquals("draw", Example.chup('p','p'));
     }
     
     @Test
     public void testChop_p1P_h_p2P_is_draw(){
         assertEquals("draw", Example.chup('h','h'));
     }
     
     @Test
     public void testChop_p1P_s_p2P_p_is_p1(){
         assertEquals("p1", Example.chup('s','p'));
     }
     
     @Test
     public void testChop_p1P_h_p2P_s_is_p1(){
         assertEquals("p1", Example.chup('h','s'));
     }
     
     @Test
     public void testChop_p1P_p_p2P_h_is_p1(){
         assertEquals("p1", Example.chup('p','h'));
     }
     
     @Test
     public void testChop_p1P_h_p2P_p_is_p2(){
         assertEquals("p2", Example.chup('h','p'));
     }
     
     @Test
     public void testChop_p1P_s_p2P_h_is_p2(){
         assertEquals("p2", Example.chup('s','h'));
     }
     
     @Test
     public void testChop_p1P_p_p2P_s_is_p2(){
         assertEquals("p2", Example.chup('p','s'));
     }
       
}
